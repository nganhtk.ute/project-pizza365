package com.devcamp.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.models.CComboMenu;
import com.devcamp.restapi.service.CComboMenuService;

@RestController
@CrossOrigin
public class CComboMenuController {
    @Autowired
    CComboMenuService cComboMenuService;

    @GetMapping("/combomenu")
    public ArrayList<CComboMenu> getComboList() {
        return cComboMenuService.getComboList();
    }
}
