package com.devcamp.restapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapi.models.CComboMenu;

@Service
public class CComboMenuService {
    public ArrayList<CComboMenu> getComboList() {
        ArrayList<CComboMenu> comboList = new ArrayList<>();

        CComboMenu menuSizeS = new CComboMenu('S', "20 cm", 2, "200g", 2, "150.000");
        CComboMenu menuSizeM = new CComboMenu('M', "25 cm", 4, "300g", 3, "200.000");
        CComboMenu menuSizeL = new CComboMenu('L', "30 cm", 8, "500g", 4, "250.000");

        comboList.add(menuSizeS);
        comboList.add(menuSizeM);
        comboList.add(menuSizeL);

        return comboList;
    }
}
