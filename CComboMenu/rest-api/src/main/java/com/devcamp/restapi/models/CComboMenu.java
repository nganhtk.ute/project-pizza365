package com.devcamp.restapi.models;

public class CComboMenu {
    private char kichCo;
    private String duongKinh;
    private int suonNuong;
    private String salad;
    private int nuocNgot;
    private String thanhTien;

    public CComboMenu(char kichCo, String duongKinh, int suonNuong, String salad, int nuocNgot, String thanhTien) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }

    public char getkichCo() {
        return kichCo;
    }

    public void setkichCo(char kichCo) {
        this.kichCo = kichCo;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuonNuong() {
        return suonNuong;
    }

    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getnuocNgot() {
        return nuocNgot;
    }

    public void setnuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public String getthanhTien() {
        return thanhTien;
    }

    public void setthanhTien(String thanhTien) {
        this.thanhTien = thanhTien;
    }

    @Override
    public String toString() {
        return "CComboMenu [size=" + kichCo + ", duongKinh=" + duongKinh + ", suonNuong=" + suonNuong + ", salad="
                + salad
                + ", soLuongNuocNgot=" + nuocNgot + ", donGia=" + thanhTien + "]";
    }

}