package com.devcamp.restapi.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
// import java.time.LocalDateTime;
// import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
// import java.util.Locale;

import org.springframework.stereotype.Service;

import com.devcamp.restapi.models.CDrink;

@Service
public class CDrinkService {
    public ArrayList<CDrink> getDrinkList() {
        // LocalDateTime ldt = LocalDateTime.now();
        // String date = DateTimeFormatter.ofPattern("dd/mm/yyyy",
        // Locale.ENGLISH).format(ldt);
        // Date dateTime = new Date(System.currentTimeMillis());
        // String date = dateTime.getDate() + "/" + (dateTime.getMonth() + 1) + "/" +
        // (dateTime.getYear() + 1900);
        ArrayList<CDrink> drinkList = new ArrayList<>();
        Date d = new Date(System.currentTimeMillis());
        DateFormat df = new SimpleDateFormat();
        df = new SimpleDateFormat("dd/MM/yyyy");
        String date = df.format(d);

        CDrink tratac = new CDrink("TRATAC", "Trà tắc", 10000, null, date, date);
        CDrink coca = new CDrink("COCA", "Cocacola", 15000, null, date, date);
        CDrink pepsi = new CDrink("PEPSI", "Pepsi", 15000, null, date, date);
        CDrink lavie = new CDrink("LAVIE", "Lavie", 5000, null, date, date);
        CDrink trasua = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, date, date);
        CDrink fanta = new CDrink("FANTA", "Fanta", 15000, null, date, date);

        drinkList.add(tratac);
        drinkList.add(coca);
        drinkList.add(pepsi);
        drinkList.add(lavie);
        drinkList.add(trasua);
        drinkList.add(fanta);

        return drinkList;
    }
}
