package com.devcamp.restapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.models.CDrink;
import com.devcamp.restapi.service.CDrinkService;

@RestController
@CrossOrigin
public class CDrinkController {
    @Autowired
    CDrinkService cDrinkService;

    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrinkList() {
        return cDrinkService.getDrinkList();
    }
}
