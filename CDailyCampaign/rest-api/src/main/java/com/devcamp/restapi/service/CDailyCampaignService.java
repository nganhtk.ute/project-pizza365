package com.devcamp.restapi.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.stereotype.Service;

@Service
public class CDailyCampaignService {

    public String getDailyDiscount() {
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        String discountMessage = "";
        // if (dtfVietNam.format(today) == "Thứ hai") {
        // discountMessage = "Hôm nay Thứ hai, mua 1 tặng 1";
        // }
        switch (dtfVietNam.format(today)) {
            case "Thứ Hai":
                discountMessage = "Hôm nay Thứ hai, mua 1 tặng 1!!!";
                break;
            case "Thứ Ba":
                discountMessage = "Hôm nay Thứ ba, tặng tất cả khách hàng một phần bánh nướng mật ong!!!";
                break;
            case "Thứ Tư":
                discountMessage = "Hôm nay Thứ tư, mua 2 tặng 1!!!";
                break;
            case "Thứ Năm":
                discountMessage = "Hôm nay Thứ năm, giảm 50% cho tất cả các loại pizza từ size L!!!";
                break;
            case "Thứ Sáu":
                discountMessage = "Hôm nay Thứ sáu, tặng tất cả khách hàng dưới 1m40 một chai nước ngọt!!!";
                break;
            case "Thứ Bảy":
                discountMessage = "Hôm nay Thứ bảy, miễn phí ship cho đơn hàng từ 500 ngàn trở lên!!!";
                break;
            case "Chủ Nhật":
                discountMessage = "Hôm nay Chủ nhật, tặng tất cả khách hàng một phần bánh ngọt khi mua tại cửa hàng!!!";
                break;
        }

        return discountMessage;
    }
}
