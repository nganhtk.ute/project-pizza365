package com.devcamp.restapi.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.service.CDailyCampaignService;

@RestController
@CrossOrigin
public class CDailyCampaignController {
    @Autowired
    CDailyCampaignService cDailyCampaignService;

    @GetMapping("/campaigns/")
    public String getDailyDiscount() {
        return cDailyCampaignService.getDailyDiscount();
    }

    // @GetMapping("/campaigns/")
    // public String getDailyDiscount() {
    // DateTimeFormatter dtfVietnam =
    // DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
    // LocalDate today = LocalDate.now(ZoneId.systemDefault());
    // return String.format("Hôm nay %s, mua 1 tặng 1.", dtfVietnam.format(today));
    // }
}
